# Lagalt

In this case, the candidates are supposed to come up with and build a distributed software solution.
* A back-end that exposes the relevant REST API endpoints
* A database (developer’s choice)

The backend of the lagalt project is what you'll find here. Using a PostgreSQL database on heroku, this project implements a REST APi.


# Heroku pages
Keycloak 
https://app-lagalt.herokuapp.com/auth
Backend
https://app-lagalt-bakend.herokuapp.com/
Frontend
https://app-lagalt-front.herokuapp.com

# Tools
* Heroku
* Docker
* intellij
* springboot Java
* Postman
* Keycloak


## Contributing
[Mohamed Abdel Monem (@Mohxmedzz)](@Mohxmedzz)
[Rinat Iunusov (@lynxxxxx)](@lynxxxxx)
[Denise Leinonen (@denise.leinonen)](@denise.leinonen)
[Hussein Mohamed (@huah1600.hmah)](@huah1600.hmah)



