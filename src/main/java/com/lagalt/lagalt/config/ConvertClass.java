package com.lagalt.lagalt.config;

import com.lagalt.lagalt.models.dtos.ProjectDTO;
import com.lagalt.lagalt.models.dtos.UserDTO;
import com.lagalt.lagalt.models.entitys.ProjectEntity;
import com.lagalt.lagalt.models.entitys.UserEntity;


import java.util.stream.Collectors;

public class ConvertClass {


    public static UserDTO convertUserDto(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getUid(),
                userEntity.getFirstname(),
                userEntity.getLastname(),
                userEntity.getEmail(),
                userEntity.getUsername(),
                userEntity.getRoles(),
                userEntity.isHide(),
                userEntity.getProject() != null ?
                        userEntity.getProject().stream()
                                .map(ConvertClass::convertProjectDto777)
                                .collect(Collectors.toList()) :
                        null,
                        userEntity.isRequestB()
//               userEntity.getProjectEntities() != null ?
//                       userEntity.getProjectEntities()
//                        .stream().map(ConvertClass::convertProjectDto)
//                        .collect(Collectors.toList())
//                : null,
//                userEntity.getRequest() != null ?
//                userEntity.getRequest().stream().map(ConvertClass::convertProjectDto)
//                        .collect(Collectors.toList())
//                        : null
        );
    }

    public static ProjectDTO convertProjectDto777(ProjectEntity projectEntity) {
        return new ProjectDTO(
                projectEntity.getProjectId(),
                projectEntity.getProjectUrl(),
                projectEntity.getProjectTypes()
        );
    }

    public static ProjectDTO convertProjectDto(ProjectEntity projectEntity) {
        return new ProjectDTO(
                projectEntity.getProjectId(),
                projectEntity.getProjectUrl(),
                projectEntity.getProjectTypes(),
                projectEntity.getApprovedRequest() != null ?
                        projectEntity.getApprovedRequest()
                                .stream().map(ConvertClass::convertUserDto)
                                .collect(Collectors.toList())
                        : null,
                projectEntity.getRequestToFollow() != null ?
                        projectEntity.getRequestToFollow()
                                .stream().map(ConvertClass::convertUserDto)
                                .collect(Collectors.toList())
                        : null,
                projectEntity.getTitle(),
                projectEntity.getDescription(),
                projectEntity.isApprove(),
                projectEntity.isRequest()
        );
    }

}


//    public static void convertUserDto2(UserEntity userEntity) {
//        new UserDTO(
//                userEntity.getFirstname(),
//                userEntity.getLastname(),
//                userEntity.getEmail(),
//                userEntity.getUsername(),
//                userEntity.getRoles()
//        );
//    }

//    public static  UserEntity convertDTOToUser(UserDTO userDTO) {
//        return  new UserEntity(
//                userDTO.getId(),
//                userDTO.getFirstname(),
//                userDTO.getLastname(),
//                userDTO.getEmail(),
//                userDTO.getUsername(),
//                userDTO.getRoles(),
//                userDTO.getProjectDTOS()
//                        .stream().map(ConvertClass::convertDTOToProject)
//                        .collect(Collectors.toList()),
//                userDTO.getRequest()
//                        .stream().map(ConvertClass::convertDTOToProject)
//                        .collect(Collectors.toList()),
//                userDTO.isHide(),
//                userDTO.getOwner_id()
//        );
//
//    }
//    public static void convertProjectDto2(ProjectEntity projectEntity) {
//        new ProjectDTO(
//                projectEntity.getProjectUrl(),
//                projectEntity.getProjectTypes(),
//                projectEntity.getTitle(),
//                projectEntity.getDescription()
//        );
//    }
//    public static ProjectEntity convertDTOToProject(ProjectDTO projectDTO) {
//        return  new ProjectEntity(
//                projectDTO.getProjectId(),
//                projectDTO.getProjectUrl(),
//                projectDTO.getProjectTypes(),
//                projectDTO.getTitle(),
//                projectDTO.getDescription(),
//                projectDTO.isApprove(),
//                projectDTO.getOwner_id()
//        );
//    }
