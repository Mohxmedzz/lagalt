package com.lagalt.lagalt.controllers;


import com.lagalt.lagalt.config.ConvertClass;
import com.lagalt.lagalt.models.dtos.ProjectDTO;
import com.lagalt.lagalt.models.entitys.ProjectEntity;
import com.lagalt.lagalt.models.entitys.UserEntity;
import com.lagalt.lagalt.services.ProjectService;
import com.lagalt.lagalt.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/project/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProjectController {


    private final UserService userService;
    private final ProjectService projectService;

    public ProjectController(UserService userService, ProjectService projectService) {
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping("/getAllProjects")
    public ResponseEntity getAllProject() {
        List<ProjectEntity> projectEntity = projectService.getAllProjects();
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));
    }


    @GetMapping("/getAllProjectsForLoggedIn/{id}")
    public ResponseEntity getAllProjectsForLoggedIn(@PathVariable String id) throws Exception {
        List<ProjectEntity> projectEntity = projectService.getAllProjectsForLoggedIn(id);
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));
    }

    @GetMapping("/getAllProjectsWithSentRequest/{id}")
    public ResponseEntity getAllProjectsWithSentRequest(@PathVariable String id) throws Exception {
        List<ProjectEntity> projectEntity = projectService.getAllProjectsWithSentRequest(id);
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));
    }
    @GetMapping("/getAllFollowedProjects/{id}")
    public ResponseEntity getAllFollowedProjects(@PathVariable String id) {
        List<ProjectEntity> projectEntity = projectService.getAllFollowedProjects(id);
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));

    }


    @GetMapping("/getAllProjectToFollow/{id}")
    public ResponseEntity getAllProjectToFollow(@PathVariable String id) {
        List<ProjectEntity> projectEntity = projectService.getAllProjectToFollow(id);
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));

    }

    @PostMapping("create-project/{id}")
    public ResponseEntity createProjectByUserId(@PathVariable String id, @AuthenticationPrincipal Jwt jwt, @RequestBody ProjectEntity projectEntity) throws Exception {
        ProjectEntity project = projectService.createProjectByUserId(id, projectEntity);
        ConvertClass.convertProjectDto(project);
        URI uri = URI.create("api/v1/project/" + id);
        return ResponseEntity.created(uri).build();
    }




}
