package com.lagalt.lagalt.controllers;

import com.lagalt.lagalt.config.ConvertClass;
import com.lagalt.lagalt.models.entitys.ProjectEntity;
import com.lagalt.lagalt.models.entitys.UserEntity;
import com.lagalt.lagalt.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/user/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/users-projects/{id}")
    public ResponseEntity getAllUsersProjects(@PathVariable String id) {
        List<ProjectEntity> projectEntity = userService.getAllUsersProjects(id);
        return ResponseEntity.ok(projectEntity.stream().map(ConvertClass::convertProjectDto));

    }


    @GetMapping("/get-one-project/{id}")
    public ResponseEntity getOneProject(@PathVariable String id) {
        ProjectEntity projectEntity = userService.getOneProject(id);
        return ResponseEntity.ok(ConvertClass.convertProjectDto(projectEntity));
    }

    @GetMapping("/get-one-user/{id}")
    public ResponseEntity getOneUser(@PathVariable String id) {
        UserEntity userEntity = userService.getOneUser(id);
        return ResponseEntity.ok(ConvertClass.convertUserDto(userEntity));
    }


    @PostMapping("register")
    public ResponseEntity addNewUserFromJwt(@AuthenticationPrincipal Jwt jwt) throws Exception {
        UserEntity user = userService.add(jwt.getClaimAsString("sub"),
                jwt.getClaimAsString("given_name"),jwt.getClaimAsString("family_name"),
                jwt.getClaimAsString("email"),jwt.getClaimAsString("preferred_username"),
                String.valueOf(jwt.getClaimAsStringList("roles")));
        ConvertClass.convertUserDto(user);
        URI uri = URI.create("api/v1/user/" + user.getUid());
        return ResponseEntity.created(uri).build();
    }


    @PostMapping("request-to-follow_project/{id}")
    public ResponseEntity requestToFollowProject(@PathVariable String id, @RequestBody ProjectEntity projectEntity) throws Exception {
       ProjectEntity project= userService.requestToFollowProject(id, projectEntity);
       // ConvertClass.convertProjectDto(project);
       // URI uri = URI.create("api/v1/user/" + id);
        return ResponseEntity.ok(ConvertClass.convertProjectDto(project));
    }

    @PutMapping("approve-user")
    public ResponseEntity approveUser( @RequestBody ProjectEntity projectEntity) throws Exception {
        ProjectEntity project = userService.approveUser(projectEntity);
        ConvertClass.convertProjectDto(project);
      //  URI uri = URI.create("api/v1/project/" + id);
        return ResponseEntity.ok(ConvertClass.convertProjectDto(project));
    }

    @PutMapping("update-project")
    public ResponseEntity updateProject(@RequestBody ProjectEntity projectEntity) throws Exception {
        ProjectEntity project = userService.updateProject( projectEntity);
        return ResponseEntity.ok(ConvertClass.convertProjectDto(project));
    }


}
