package com.lagalt.lagalt.models.dtos;

import com.lagalt.lagalt.models.enums.ProjectTypes;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProjectDTO {

    private String projectId;


    private String projectUrl;

    private ProjectTypes projectTypes;


    private List<UserDTO> approvedRequest;


    private List<UserDTO> requestToFollow;



    private String title;


    private String description;


    private boolean approve;

    private boolean request;





    public ProjectDTO(String projectId, String projectUrl, ProjectTypes projectTypes,
                      String title, String description, boolean approve) {
        this.projectId = projectId;
        this.projectUrl = projectUrl;
        this.projectTypes = projectTypes;
        this.title = title;
        this.description = description;
        this.approve = approve;

    }

    public ProjectDTO(String projectId, String projectUrl, ProjectTypes projectTypes) {
        this.projectId = projectId;
        this.projectUrl = projectUrl;
        this.projectTypes = projectTypes;
    }

    public ProjectDTO(String projectId,String projectUrl, ProjectTypes projectTypes,
                      List<UserDTO> approvedRequest, List<UserDTO> requestToFollow,
             String title, String description, boolean approve, boolean request) {
        this.projectId = projectId;
        this.projectUrl = projectUrl;
        this.projectTypes = projectTypes;
        this.approvedRequest = approvedRequest;
        this.requestToFollow = requestToFollow;
        this.title = title;
        this.description = description;
        this.approve = approve;
        this.request = request;

    }

//    public ProjectDTO(String projectUrl, ProjectTypes projectTypes,
//                      String title, String description) {
//        this.projectUrl = projectUrl;
//        this.projectTypes = projectTypes;
//        this.title = title;
//        this.description = description;
//    }
}
