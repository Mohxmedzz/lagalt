package com.lagalt.lagalt.models.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO {
    private String uid;
    private String firstname;
    private String lastname;
    private String email;
    private String username;
    private String roles;

//    private List<ProjectDTO> projectDTOS;
//
//    private List<ProjectDTO> request;
    private boolean hide;

    private List<ProjectDTO> projectDTO;

    private boolean requestB;

    public UserDTO(String uid, String firstname, String lastname, String email,
                   String username, String roles, boolean hide, List<ProjectDTO> projectDTO, boolean requestB) {
        this.uid = uid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.username = username;
        this.roles = roles;
        this.hide = hide;
        this.projectDTO = projectDTO;
        this.requestB = requestB;
    }

    public UserDTO(String firstname, String lastname,
                   String email, String username, String roles) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.username = username;
        this.roles = roles;


    }

//    public UserDTO(String firstname, String lastname, String email, String username, String roles) {
//        this.firstname = firstname;
//        this.lastname = lastname;
//        this.email = email;
//        this.username = username;
//        this.roles = roles;
//    }
}
