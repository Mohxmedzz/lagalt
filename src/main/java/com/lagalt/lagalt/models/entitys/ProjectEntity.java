package com.lagalt.lagalt.models.entitys;

import com.lagalt.lagalt.models.enums.ProjectTypes;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ProjectEntity {

    @Id
    @Column(name = "project_id")
    private String projectId;

    @Column(name = "project_url")
    private String projectUrl;

    @Column(name = "project_type")
    @Enumerated(EnumType.STRING)
    private ProjectTypes projectTypes;

    @ManyToMany(mappedBy = "approveList")
    private List<UserEntity> approvedRequest;


    @ManyToMany(mappedBy = "request")
    private List<UserEntity> requestToFollow;

    @Column(name = "project_title")
    private String title;

    @Column(name = "project_description")
    private String description;

    @Column(name = "approve")
    private boolean approve;

    @Column(name = "request")
    private boolean request;

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;*/
/*    @OneToOne(mappedBy = "owner_id")
    @Column(name = "owner_id")
    private OwnerEntity ownerId;*/
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name= "owner_id")
private UserEntity owner;

    public void addToRequestList(UserEntity userEntity) {
        requestToFollow.add(userEntity);
    }

    public void addToApproveList(UserEntity userEntity) {
        approvedRequest.add(userEntity);
    }
    public void removeFromRequestList(UserEntity userEntity) {
        requestToFollow.remove(userEntity);
    }
}
