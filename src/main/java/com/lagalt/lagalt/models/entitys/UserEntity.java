package com.lagalt.lagalt.models.entitys;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity {

    @Id
    @Column(name = "user_id")
    private String uid;

    @Column(name = "user_firstName")
    private String firstname;

    @Column(name = "user_lastName")
    private String lastname;

    @Column(name = "user_email")
    private String email;

    @Column(name = "user_username")
    private String username;

    @Column(name = "role")
    private String roles;

    @ManyToMany()
    @JoinTable(name = "collaborateurs",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")})
    private List<ProjectEntity> approveList;

    @ManyToMany
    @JoinTable(name = "user_project_request",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")})
    private List<ProjectEntity> request;

    @Column(name = "hide")
    private boolean hide;

    @Column(name = "requestb")
    private boolean requestB;



    @OneToMany(mappedBy = "owner",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Column(name = "projects")
    private List<ProjectEntity> project;


    public void addToRequestList(ProjectEntity projectEntity) {
        request.add(projectEntity);
    }

    public void addToApproveList(ProjectEntity projectEntity) {
        approveList.add(projectEntity);
    }

    public void removeFromRequestList(ProjectEntity projectEntity){
        request.remove(projectEntity);
    }

}
