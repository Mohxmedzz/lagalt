package com.lagalt.lagalt.models.enums;

public enum ProjectTypes {
    MUSIC,
    MOVIE,
    GAMEDEVELOPMENT,
    WEBDEVELOPMENT
}
