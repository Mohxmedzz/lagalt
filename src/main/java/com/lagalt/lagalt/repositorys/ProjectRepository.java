package com.lagalt.lagalt.repositorys;

import com.lagalt.lagalt.models.entitys.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity, String> {
}
