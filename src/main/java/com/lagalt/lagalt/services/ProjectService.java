package com.lagalt.lagalt.services;

import com.lagalt.lagalt.models.entitys.ProjectEntity;
import com.lagalt.lagalt.models.entitys.UserEntity;
import com.lagalt.lagalt.repositorys.ProjectRepository;
import com.lagalt.lagalt.repositorys.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProjectService {

    ProjectRepository projectRepository;
    UserRepository userRepository;


    public ProjectEntity createProjectByUserId(String id, ProjectEntity projectEntity) throws Exception {
        UserEntity user = userRepository.findById(id).get();
        if (!userRepository.existsById(id))
            throw new Exception("user not exist");
        ProjectEntity projectEntity1 = new ProjectEntity();
        projectEntity1.setProjectId(UUID.randomUUID().toString());
        projectEntity1.setProjectUrl(projectEntity.getProjectUrl());
        projectEntity1.setProjectTypes(projectEntity.getProjectTypes());
        projectEntity1.setTitle(projectEntity.getTitle());
        projectEntity1.setDescription(projectEntity.getDescription());
        projectEntity1.setOwner(user);

        return projectRepository.save(projectEntity1);

    }

    public List<ProjectEntity> getAllProjects() {
        return projectRepository.findAll();
    }


    public List<ProjectEntity> getAllProjectsForLoggedIn(String id) {
        UserEntity userEntity = userRepository.findById(id).get();
        return projectRepository.
                findAll().stream().filter(a -> !a.getOwner().getUid().equals(id)
                        && !a.getApprovedRequest().contains(userEntity)
                        && !a.getRequestToFollow().contains(userEntity))
                .collect(Collectors.toList());
    }

    public List<ProjectEntity> getAllProjectsWithSentRequest(String id) {
        UserEntity userEntity = userRepository.findById(id).get();
        return projectRepository.
                findAll().stream().filter(a -> !a.getOwner().getUid().equals(id)
                        && a.getRequestToFollow().contains(userEntity))
                .collect(Collectors.toList());
    }
    public List<ProjectEntity> getAllProjectToFollow(String id) {
        UserEntity userEntity = userRepository.findById(id).get();
//        return projectRepository.findAll()
//                .stream().filter(a -> a.getRequestToFollow().size() > 0).collect(Collectors.toList());
        return projectRepository.findAll().stream().filter(a -> a.getOwner().getUid().equals(id)
                        && !a.getRequestToFollow().contains(userEntity) && a.getRequestToFollow().size() > 0)
                .collect(Collectors.toList());
    }

    public List<ProjectEntity> getAllFollowedProjects(String id) {
        return userRepository.findById(id).get().getApproveList();
    }


}

