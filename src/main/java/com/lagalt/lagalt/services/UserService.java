package com.lagalt.lagalt.services;

import com.lagalt.lagalt.models.entitys.ProjectEntity;
import com.lagalt.lagalt.models.entitys.UserEntity;
import com.lagalt.lagalt.repositorys.ProjectRepository;
import com.lagalt.lagalt.repositorys.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {
    UserRepository userRepository;
    ProjectRepository projectRepository;

    public UserEntity add(String uid, String firstname, String lastname, String email, String username, String roles) throws Exception {
        if (userRepository.existsById(uid)) {
            return userRepository.findById(uid).get();
        }

        // Create new user
        UserEntity user = new UserEntity();
        user.setUid(uid);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setEmail(email);
        user.setUsername(username);
        user.setRoles(roles);
        user.setRequestB(false);

        return userRepository.save(user);

    }



    public ProjectEntity requestToFollowProject(String id, ProjectEntity projectEntity) throws Exception {
        UserEntity user = userRepository.findById(id).get();
        ProjectEntity project = projectRepository.findById(projectEntity.getProjectId()).get();
        if (!project.getRequestToFollow().contains(user)) {
            user.setRequestB(true);
            project.setApprove(false);
            project.setRequest(true);
            project.addToRequestList(user);
            user.addToRequestList(project);
        } else {
            throw new Exception("Request have been sent earlier");
        }
        return projectRepository.save(project);
    }

    public List<ProjectEntity> getAllUsersProjects(String id) {
        UserEntity userEntity = userRepository.findById(id).get();
        return projectRepository.
                findAll().stream().filter(a -> a.getOwner().getUid().equals(userEntity.getUid()))
                .collect(Collectors.toList());
    }

    public ProjectEntity getOneProject(String id) {
        return projectRepository.findById(id).get();
    }



    public ProjectEntity approveUser(ProjectEntity projectEntity) {
        ProjectEntity project = projectRepository.findById(projectEntity.getProjectId()).get();
        List<UserEntity> requestList = project.getRequestToFollow();

        boolean contains;
        for (UserEntity user : requestList) {
            contains = requestList.stream().anyMatch(user1 -> user1.getUid() == user.getUid());
            if (contains) {
                // approveList.add(user);
                project.addToApproveList(user);
                project.removeFromRequestList(user);
                user.addToApproveList(project);
                user.removeFromRequestList(project);
                project.setApprove(true);
                project.setRequest(false);
                projectRepository.save(project);
                userRepository.save(user);
                break;
            }

        }
        projectRepository.save(project);
        return project;
    }

    public ProjectEntity updateProject( ProjectEntity projectEntity) throws Exception{
        //UserEntity user = userRepository.findById(id).get();

        ProjectEntity project= projectRepository.findById(projectEntity.getProjectId()).get();
        project.setTitle(projectEntity.getTitle());
        project.setDescription(projectEntity.getDescription());
        project.setProjectUrl((projectEntity.getProjectUrl()));
        return projectRepository.save(project);
    }

    public UserEntity getOneUser(String id) {
        return userRepository.findById(id).get();
    }
}
