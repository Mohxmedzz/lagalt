

INSERT INTO user_entity (user_id, user_email, user_first_name, hide, user_last_name, requestb, role, user_username)
VALUES ('575d591c-fb16-4cbe-a541-5a98af236ca1', 'david@gmail.com', 'David', 'false', 'Svensson', 'false', '[USER]',
        'david.svensson');

INSERT INTO project_entity(project_id, approve, project_description,owner_id, project_type, project_url, request,
                           project_title)
VALUES ('37c1c55d-65c5-42c5-a4cd-a744c029273c', 'false', 'Lord of the rings is a fantasy movie about a companion who is trying to destroy the ring of Sauron.', '575d591c-fb16-4cbe-a541-5a98af236ca1',
        'MOVIE', 'https://www.imdb.com/title/tt0120737/', 'false', 'Lord of the rings');
INSERT INTO project_entity(project_id, approve, project_description, owner_id, project_type, project_url, request,
                           project_title)
VALUES ('37c1c55d-65c5-42c5-a4cd-a744c029271s', 'false', 'The avengers is about a team of extraordinary individuals, with either superpowers or other special characteristics.', '575d591c-fb16-4cbe-a541-5a98af236ca1',
        'MOVIE', 'https://www.marvel.com/movies/avengers-endgame', 'false', 'Avengers');


INSERT INTO user_entity (user_id, user_email, user_first_name, hide, user_last_name,requestb, role, user_username)
VALUES ('11b12c81-5353-46e7-a819-93b9f3410837', 'lina@gmail.com', 'Lina', 'false', 'Andersson','false', '[USER]',
        'lina.andersson');

INSERT INTO project_entity(project_id, approve, project_description, owner_id, project_type, project_url, request,
                           project_title)
VALUES ('37c1c55d-65c5-42c5-a4cd-a744c029272b', 'false', 'The Fast & the Furious är en serie amerikanska actionfilmer som bland annat fokuserar på illegal street racing.', '11b12c81-5353-46e7-a819-93b9f3410837',
        'MOVIE', 'https://www.imdb.com/title/tt0232500/', 'false', 'The Fast & the Furious')